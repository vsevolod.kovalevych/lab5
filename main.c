#include "Driver_SPI.h"
//#include "stm32f4xx.h"
#include "stm32f4xx_hal_gpio.h"
#include "stm32f4xx_hal_rcc.h"

//extern ARM_DRIVER_SPI Driver_SPI1;
//ARM_DRIVER_SPI* SPIdrv = &Driver_SPI1;
	
//void Touch_Read (uint8_t reg, uint8_t* val) 
//{
//	HAL_GPIO_WritePin(GPIOE, GPIO_PIN_3, GPIO_PIN_RESET);
//	reg = reg | 0x80;
//	while (SPIdrv->GetStatus().busy) { 
//	} 
//  SPIdrv->Send(&reg, 1); 
//	while (SPIdrv->GetStatus().busy);
//	SPIdrv->Receive(val, 1);
//	while (SPIdrv->GetStatus().busy);
//	HAL_GPIO_WritePin(GPIOE, GPIO_PIN_3, GPIO_PIN_SET);
//}

//uint8_t val;
//float X, Y, Z;
//	
//void ReadAxes() {
//	uint8_t buffer[6];
//	Touch_Read(0x28, &buffer[0]);
//	Touch_Read(0x29, &buffer[1]);
//	Touch_Read(0x2A, &buffer[2]);
//	Touch_Read(0x2B, &buffer[3]);
//	Touch_Read(0x2C, &buffer[4]);
//	Touch_Read(0x2D, &buffer[5]);
//	X = (int16_t)((buffer[1] << 8) + buffer[0]) * 0.06;
//	Y = (int16_t)((buffer[3] << 8) + buffer[2]) * 0.06;
//	Z = (int16_t)((buffer[5] << 8) + buffer[4]) * 0.06;
//}

//int main() {
//	__HAL_RCC_GPIOA_CLK_ENABLE();
//	//__HAL_RCC_GPIOE_CLK_ENABLE();
//	
//	GPIO_InitTypeDef GPIO_InitStructure;
//	
//	GPIO_InitStructure.Mode =  GPIO_MODE_OUTPUT_PP;
//	GPIO_InitStructure.Pin = GPIO_PIN_3;
//	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_LOW;
//	
//	HAL_GPIO_Init(GPIOE, &GPIO_InitStructure);
//	HAL_GPIO_WritePin(GPIOE, GPIO_PIN_3, GPIO_PIN_SET);
//	
//	SPIdrv->Initialize(NULL);
//	SPIdrv->PowerControl(ARM_POWER_FULL);
//	
//	SPIdrv->Control(ARM_SPI_MODE_MASTER | ARM_SPI_CPOL1_CPHA1 | ARM_SPI_MSB_LSB | ARM_SPI_SS_MASTER_UNUSED | ARM_SPI_DATA_BITS(8), 2000000);
//	
//	Touch_Read(0x0F, &val);
//	ReadAxes();
//	
//}

uint16_t delay_c = 0; 

void SysTick_Handler(void){
	if(delay_c > 0)
		delay_c--;
}

void delay_ms(uint16_t delay_t)
{
	delay_c = delay_t;
	while(delay_c){};
}

void LCD5110_GPIO_Config()
{
	GPIO_InitTypeDef GPIOA_Init;
	GPIO_InitTypeDef GPIOC_Init;
	
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOC_CLK_ENABLE();
	
	GPIOA_Init.Pin = GPIO_PIN_8;
	GPIOA_Init.Speed = GPIO_SPEED_FREQ_MEDIUM;
	GPIOA_Init.Mode = GPIO_MODE_OUTPUT_PP;
	
	GPIOC_Init.Pin = GPIO_PIN_6 | GPIO_PIN_7 | GPIO_PIN_8 | GPIO_PIN_9;
	GPIOA_Init.Speed = GPIO_SPEED_FREQ_MEDIUM;
	GPIOA_Init.Mode = GPIO_MODE_OUTPUT_PP;
	
	HAL_GPIO_Init(GPIOA, &GPIOA_Init);
	HAL_GPIO_Init(GPIOC, &GPIOC_Init);
}

void LCD5110_CS(unsigned char OnOff)
{
	if (OnOff)
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_9, GPIO_PIN_SET);
	else 
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_9, GPIO_PIN_RESET);
}

void LCD5110_RST(unsigned char OnOff)
{
	if (OnOff)
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_SET);
	else 
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_RESET);
}

void LCD5110_DC(unsigned char OnOff)
{
	if (OnOff)
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, GPIO_PIN_SET);
	else 
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, GPIO_PIN_RESET);
}

void LCD5110_MO(unsigned char OnOff)
{
	if (OnOff)
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6, GPIO_PIN_SET);
	else 
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6, GPIO_PIN_RESET);
}

void LCD5110_SCK(unsigned char OnOff)
{
	if (OnOff)
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_SET);
	else 
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_RESET);
}

void LCD5110_Write_Byte(unsigned char data, unsigned char mode) {
	unsigned char i;
	
	LCD5110_CS(0); // Select chip
	if (!mode) {
		LCD5110_DC(0);
	}
	else {
		LCD5110_DC(1); 
	}
	
	for (i = 0; i < 8; i++) {
		LCD5110_MO(data & 0x80); 
		data = data << 1;
		LCD5110_SCK(0);		
		LCD5110_SCK(1);
	}
	LCD5110_CS(1);
}

void LCD_init()
{ 
	LCD5110_GPIO_Config();

	LCD5110_DC(1);//LCD_DC = 1;
	LCD5110_MO(1);//SPI_MO = 1;
	LCD5110_SCK(1);//SPI_SCK = 1;
	LCD5110_CS(1);//SPI_CS = 1;
	
	LCD5110_RST(0);//LCD_RST = 0;
	delay_ms(10);
	LCD5110_RST(1);//LCD_RST = 1;

	LCD5110_Write_Byte(0x21, 0);
	LCD5110_Write_Byte(0xc6, 0);
	LCD5110_Write_Byte(0x06, 0);
	LCD5110_Write_Byte(0x13, 0);
	LCD5110_Write_Byte(0x20, 0);
	LCD5110_Write_Byte(0x0c,0);
}

void LCD5110_Display_Clear() 
	{
	// Set X Y to 0 0
	LCD5110_Write_Byte(0x80, 1);
	int size = 84 * 48 / 8;
	int i;
	for (i = 0; i < size; i++) {
		LCD5110_Write_Byte(0x00, 1); 
	}
}

int main() {
	LCD_init();
	LCD5110_Display_Clear();
}
